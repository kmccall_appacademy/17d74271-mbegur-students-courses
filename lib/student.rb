class Student

  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name.capitalize
    @last_name = last_name.capitalize
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end


  def enroll(course)
    @courses.each do |el|
      raise "error" if course.conflicts_with?(el)
    end
    
    @courses << course unless @courses.include?(course)
    course.students << self

  end

  def course_load
    hash = Hash.new(0)
    @courses.each do |course|
      hash[course.department] += course.credits
    end
    hash
  end

end
